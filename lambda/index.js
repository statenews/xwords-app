// // import Crossword from "puzzler";
const {
    S3Client,
    PutObjectCommand,
    GetObjectCommand,
    HeadObjectCommand,
} = require("@aws-sdk/client-s3");
const { fromIni } = require("@aws-sdk/credential-provider-ini");
const Crossword = require("puzzler").Crossword;

const exampleData =
    "6xRBQ1JPU1MmRE9XTgAC2kvO+9GbV3kdMS4yAAAAAAAAAAAAAAAAAAAAAAAPD0wAAQAAAFBIRFMuSUJJRC5TTE9CU0xPT1AuTUVTQS5USUFSQU9SRU8uUEFOUy5SRVRBR0RBUlRNT1VUSE5JR0hULi4uLk9VU1QuLk9ORS4uLlNUSUZMRS5TV0lHLlRMQUFPUlRBLkxBT1MuU0hJTkdSRUVOS0VZV0VFS0VOREVUTkEuUk9TUy5WRVJERVNFRS5MRU5PLlBJUEVUUy4uLlNBTS4uTUVBVC4uLi5XSU5URVJDQVJOSVZBTE1BTEVLLkFJUlMuQ0VOQUFHSUxFLlRBQ08uQUdFRFRFQUxTLlNPT04uTEFXUy0tLS0uLS0tLS4tLS0tLS0tLS0uLS0tLS4tLS0tLS0tLS0uLS0tLS4tLS0tLS0tLS0tLS0tLS0tLS0tLi4uLi0tLS0uLi0tLS4uLi0tLS0tLS4tLS0tLi0tLS0tLS0tLi0tLS0uLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0uLS0tLS4tLS0tLS0tLS4tLS0tLi0tLS0tLS4uLi0tLS4uLS0tLS4uLi4tLS0tLS0tLS0tLS0tLS0tLS0tLi0tLS0uLS0tLS0tLS0tLi0tLS0uLS0tLS0tLS0tLi0tLS0uLS0tLURhcnRtb3V0aCBUcmFkaXRpb25zACAgTmluYSBTbG9hbiAgAKkgVGhlIERhcnRtb3V0aABEZWdzLiBmb3IgbWFueSBhIERhcnRtb3V0aCBwcm9mAFRydWRnZQBCYXQgTWl0enZhaCBkYW5jAEdvLWdldHRlcgBBZnRlcm5vb24gcmVmcmVzaG1lbnQsIGZvciBhIEJyaXQARm9vdG5vdGUgYWJici4ASW50cnVkAEZpbmUgc3BlY2ltZW4AIl9fX18gaXQgaXJvbmljPyIgKDE5OTYgTW9yaXNzZXR0ZSBseXJpYykAQm9sAE1lc3N5IHR5cGVzAFRocmVhZAAiTXkgX19fX18iIChyZXNwZWN0ZnVsIGdyZWV0aW5nKQBWb3cAU3BvaWxlZCBraQBEcm9vcABXYWxrIGFyb3VuZCBPY2NhbSwgZm9yIG9uZQBGbGF0LXRvcHBlZCBoaWxsAFByaW5jZXNzJyBoZWFkd2VhcgBDaG9jb2xhdGUgc2FuZHdpY2ggY29va2llAFNoYWtlcyAob3V0KQBJZGVudGlmeSBhZ2FpbiwgYXMgaW4gYW4gSW5zdGFncmFtIHBvc3QASG9tZWNvbWluZyB0cmFkaXRpb24gd2l0aCBhIGJvbmZpcmUARGlzbmV5IHByaW5jZXNzIHdobyBoYWQgdG8gYmUgYXMgIm15c3RlcmlvdXMgYXMgdGhlIGRhcmsgc2lkZSBvZiB0aGUgbW9vbiIARGluAFJlbW92ZSBmcm9tIHBvd2VyAFRocmVlIG1pbnVzIHR3bwBTbW90aGVyAEVsZGVycywgb2Z0ZW4AUmljaCBjYWtlAFNoZXJsb2NrJ3MgaW50ZWxsZWN0dWFsIG1hdGNoLCBub3RhYmx5AERyaW5rLCBhcyBhIEtleXN0b25lAERvamEgQ2F0IHNpbmdsZSB3aXRoIGEgdmlyYWwgVGlrVG9rIGRhbmMAQXN0b25pc2hlcwBEaXN0cmlidXRpdmUgcmVxdWlyZW1lbnQgc2F0aXNmaWVkIGJ5IG1hbnkgQ1MgY2xhc3NlcwAiVGhhdCB3YXkhIgBCaWcgbmFtZSBhbW9uZ3N0IGNob2NvbGF0aWVycwBSYW5nZSB0aHJvdWdoIFBlcnUASW1wb3J0YW50IGFydGVyeQBMYW5kbG9ja2VkIFNvdXRoZWFzdCBBc2lhbiBjb3VudHJ5AEtpbmdzIG9mIF9fX18gKEFtZXJpY2FuIHJvY2sgYmFuZCkAQm9keSBwYXJ0IHRoYXQgY2FuIHN1ZmZlciBmcm9tICJzcGxpbnRzIgBEdWJpb3UAU3ByaW5nIHRyYWRpdGlvbiB3aXRoIGEgbXVzaWMgZmVzdGl2YWwAS3Jpc3B5IF9fX19fAEZpamkgcml2YWwASXRhbGlhbiBtb3VudABHZWxsZXIgb2YgIkZyaWVuZHMiAFNhbHNhIF9fX19fIChncmVlbiBzYXVjZSkAT2JzZXJ2ZQBKYXkgZm9ybWVybHkgb2YgbGF0ZS1uaWdodABIYW5udWthaCBkZWxpY2FjaWVzAExhYiBkcm9wcGVycwBIdW1hbiBiZWluZwBQdWNrZXR0IG9mICJpQ2FybHkiAEZpc2hob29rIGF0dGFjaG1lbnQAQ2hpY2tlbiBvciBiZWVmAFBvbG8ncyBwYXJ0bmVyAERhcnRtb3V0aCB0cmFkaXRpb24gd2l0aCBhIHBvbGFyIHBsdW5nZQBUaGUgbWluaW11bSBvbmUgaW4gTmV3IEhhbXBzaGlyZSBpcyAkNy4yNQBIaXAgYm9uZXMAUmVteSBhbmQgRW1pbGxlLCBpbiAiUmF0YXRvdWlsbGUiAEl0YWxpYW4gZ3JlZXRpbmcAVG9yaSBvciBUcmluYSBvZiAiVmljdG9yaW91cyIARnJlc2hseQBCbG9rZXMAQWN0b3IgUmFtaSBvZiAiQm9oZW1pYW4gUmhhcHNvZHkiAFlvZ2EgY2xhc3MgbmVjZXNzaXR5AFByZW1pZXJlcwBXcmVzdGxlciBKb2huAE5pbWJsZQBDaGlwb3RsZSBvcmRlcgBHcmV3IG9sZGVyAEdyZWVuaXNoLWJsdWVzAEFub24AUnVsZQAATFRJTQMAUwAwLDEA";

const _main = function () {
    _handler(
        { puzzleData: exampleData, client: "test", date: "2020-01-01" },
        {}
    );
};

const _configureAws = function () {
    let client;
    if (_isLocal()) {
        client = new S3Client({
            region: "us-east-1",
            credentials: fromIni({ profile: "lambda" }),
        });
    } else {
        client = new S3Client({ region: "us-east-1" });
    }
    return client;
};

const _isLocal = function () {
    return process.argv[1] != "/var/runtime/index.js";
};

const _handler = async function (event, context) {
    console.log("Running Puzzle Handler");
    const c = new Crossword({
        raw: Buffer.from(event.puzzleData, "base64"),
    });
    const jsonData = c.toJson();

    console.log("Building Data");

    let parsedClues = { across: {}, down: {} };
    for (const [key, value] of Object.entries(jsonData.clues.across)) {
        const row = jsonData.board[value.row];
        let answer = [];
        let start = value.col;
        for (let i = start; i < row.length; i++) {
            if (row[i].isBlank) {
                break;
            }
            answer.push(row[i].letter);
        }

        parsedClues.across[key] = value;
        parsedClues.across[key]["answer"] = answer.join("");
    }

    for (const [key, value] of Object.entries(jsonData.clues.down)) {
        const start = value.row;
        let answer = [];
        let col = value.col;
        for (let i = start; i < jsonData.board.length; i++) {
            if (jsonData.board[i][col].isBlank) {
                break;
            }
            answer.push(jsonData.board[i][col].letter);
        }

        parsedClues.down[key] = value;
        parsedClues.down[key]["answer"] = answer.join("");
    }

    const clientCode = event.client.toLowerCase().replace(/[^a-z\-]/i, "");
    let date;
    if (event.date.match(/\d{4}-\d{2}-\d{2}/, "")) {
        date = event.date.match(/\d{4}-\d{2}-\d{2}/, "")[0];
    } else {
        date = "9999-99-99";
    }

    const fileName = jsonData.header.title
        .replace(/ /, "-")
        .replace(/[^a-zA-Z0-9\-\.]/, "")
        .toLowerCase();

    console.log("Saving to S3");
    const saveData = Object.assign(jsonData, {}, { parsedClues: parsedClues });

    const client = _configureAws();
    const status = await client.send(
        new PutObjectCommand({
            Bucket: "static.getsnworks.com",
            Key:
                "js/xword-data/" +
                clientCode +
                "/" +
                date +
                "-" +
                fileName +
                ".json",
            Body: JSON.stringify(saveData),
        })
    );

    console.log("Updating Manifest");
    let fileData = {};
    const updateManifest = function (data) {
        console.log("UpdateManifest");

        const manifestData = {};
        manifestData[fileName] = {
            title: jsonData.header.title,
            file: fileName + ".json",
            date: date,
        };
        const fileData = Object.assign(data, {}, manifestData);
        client.send(
            new PutObjectCommand({
                Bucket: "static.getsnworks.com",
                Key: "js/xword-data/" + clientCode + "/" + "manifest.json",
                Body: JSON.stringify(fileData),
            })
        );
    };

    try {
        const data = await client.send(
            new GetObjectCommand({
                Bucket: "static.getsnworks.com",
                Key: "js/xword-data/" + clientCode + "/manifest.json",
            })
        );

        const chunks = [];
        data.Body.on("data", (c) => {
            chunks.push(c.toString());
            updateManifest(JSON.parse(chunks.join("")));
        });
    } catch (e) {
        fileData = {};
        updateManifest(fileData);
    }
};

if (process.argv[1] != "/var/runtime/index.js") {
    _main();
}

exports.handler = _handler;
