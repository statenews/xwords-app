import React from "react";
import Crossword from "@jaredreisinger/react-crossword";
import Config from "./config.json";
import "./App.css";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            puzzleData: null,
            puzzleList: null,
            clientCode: null,
            slug: null,
        };

        this.baseUrl = (
            window.location.origin +
            "/" +
            window.location.pathname
        ).replace(/((?<!:)\/{2,})/, "/");
    }

    getQuery() {
        const q = window.location.search;
        if (!q) {
            return;
        }

        const parsed = q
            .substr(1)
            .split("&")
            .map(function (v) {
                return v.split("=");
            });

        let query = {};
        for (let i = 0; i < parsed.length; i++) {
            query[parsed[i][0]] = parsed[i][1];
        }

        return query;
    }

    buildQuery(data, refresh = false) {
        const q = this.getQuery();
        let newQuery = data;
        if (!refresh) {
            newQuery = Object.assign(q, {}, data);
        }

        const out = Object.keys(newQuery).map((k) => {
            return k + "=" + newQuery[k];
        });

        return out.join("&");
    }

    componentDidMount() {
        const q = this.getQuery();
        if (!q) {
            return;
        }
        const slug = q._c;
        const client = q._l;

        if (!client || !Config.validClients[client]) {
            return;
        }
        this.setState({ clientCode: client, slug: slug }, () => {
            if (client && !slug) {
                this.loadPuzzleList.call(this);
            } else if (client && slug) {
                this.loadPuzzleData.call(this);
            }
        });
    }

    loadPuzzleList() {
        const client = this.state.clientCode;
        async function getList() {
            const r = await fetch(
                "https://s3.amazonaws.com/static.getsnworks.com/js/xword-data/" +
                    client +
                    "/manifest.json",
                {
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            const t = await r.json();
            return t;
        }
        getList().then((data) => {
            this.setState({ puzzleList: Object.assign({}, data) });
        });
    }

    loadPuzzleData() {
        const client = this.state.clientCode;
        const slug = this.state.slug;
        async function getRemote() {
            const r = await fetch(
                "https://s3.amazonaws.com/static.getsnworks.com/js/xword-data/" +
                    client +
                    "/" +
                    slug +
                    ".json",
                {
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            const t = await r.json();
            return t;
        }
        getRemote().then((data) => {
            this.setState({ puzzleData: Object.assign({}, data) });
        });
    }

    handlePuzzleSelect(key) {
        window.location.replace(
            this.baseUrl + "?" + this.buildQuery({ _c: key })
        );
    }

    render() {
        if (this.state.puzzleData) {
            return (
                <div className="App">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                <h1>{this.state.puzzleData.header.title}</h1>
                            </div>
                            <div class="text-right">
                                <a
                                    href={
                                        this.baseUrl +
                                        "?" +
                                        this.buildQuery(
                                            { _l: this.state.clientCode },
                                            true
                                        )
                                    }
                                    class="btn btn-primary btn-sm"
                                >
                                    Back to Puzzle List
                                </a>
                            </div>
                        </div>
                    </div>
                    <Crossword
                        data={this.state.puzzleData.parsedClues}
                        columnBreakpoint="600px"
                        useStorage={false}
                    />
                </div>
            );
        } else if (
            this.state.puzzleList &&
            Object.entries(this.state.puzzleList).length
        ) {
            return (
                <div className="App">
                    <h3>Select a puzzle</h3>
                    <ul>
                        {Object.entries(this.state.puzzleList).map(
                            ([key, val]) => {
                                return (
                                    <li
                                        key={key}
                                        onClick={this.handlePuzzleSelect.bind(
                                            this,
                                            val.date + "-" + key
                                        )}
                                        className="faux-link"
                                    >
                                        {val.date} - {val.title}
                                    </li>
                                );
                            }
                        )}
                    </ul>
                </div>
            );
        } else if (!this.state.clientCode) {
            return (
                <div className="App">
                    <div style={{ textAlign: "center" }}>
                        Hrm... something went wrong...
                        <br />
                    </div>
                </div>
            );
        }

        return (
            <div className="App">
                <div style={{ textAlign: "center" }}>
                    Loading...
                    <br />
                    <img
                        src="//s3.amazonaws.com/static.getsnworks.com/placeholder/assets/img/cube-flat.png"
                        className="loader"
                        alt="Loading..."
                    />
                </div>
            </div>
        );
    }
}

export default App;
